package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

/**
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    final List<T> vertices = new ArrayList<>();
    int j;
    
    public int getN(){
        
        return vertices.size();
    }

    /**
     * @param i
     */
    public T get(int i)
    {
        if(i > vertices.size() || i < 0  )
        {
            throw new RuntimeException("get("+i+"): índice inválido");
        }
        return vertices.get(i);
    }

    /**
     * @param i
     * @param p
     */
    public void set(int i, T p)
    {
        if(i < 0 || i > vertices.size())
            throw new RuntimeException("set("+i+"): índice inválido");
        else if (vertices.size() == i)
            vertices.add(p);
        else
            vertices.set(i, p);       
    }
    public double getComprimento()
    {
        Iterator<T> it = vertices.iterator();
        double d, s = 0;
        T p, q = it.next();
        while(it.hasNext())
        {
            p = q;
            q = it.next();
            d = Math.sqrt(Math.pow(p.getX()-q.getX(), 2)+Math.pow(p.getY()-q.getY(), 2)+Math.pow(p.getZ()-q.getZ(), 2));
            s += d;
        }
        return s;
    }     
}