package utfpr.ct.dainf.pratica;

public class PontoXZ extends Ponto2D {

    public PontoXZ() {
    }

    public PontoXZ(double x, double z) {
        super(x, 0, z);
    }

    @Override
    public String toString() {
        return String.format("%s(%f,%f)", getNome(), getX(), getZ());
    }
    
}
