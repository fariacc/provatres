import utfpr.ct.dainf.pratica.PoligonalFechada;
import utfpr.ct.dainf.pratica.PontoXZ;

public class Pratica {

    public static void main(String[] args) {
        PoligonalFechada pf = new PoligonalFechada();
        pf.set(0, new PontoXZ(3, -2));
        pf.set(1, new PontoXZ(-3, 6));
        pf.set(2, new PontoXZ(0,2));
        System.out.println("Comprimento da poligonal = "+ pf.getComprimento());

    }
    
}