package utfpr.ct.dainf.pratica;

public class PontoYZ extends Ponto2D {

    public PontoYZ() {
    }

    public PontoYZ(double y, double z) {
        super(0, y, z);
    }
    
    @Override
    public String toString() {
        return String.format("%s(%f,%f)", getNome(), getY(), getZ());
    }

}
