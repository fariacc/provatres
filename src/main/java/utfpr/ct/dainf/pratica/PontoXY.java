package utfpr.ct.dainf.pratica;

public class PontoXY extends Ponto2D {

    public PontoXY() {
    }

    public PontoXY(double x, double y) {
        super(x, y, 0);
    }
    
    @Override
    public String toString() {
        return String.format("%s(%f,%f)", getNome(), getX(), getY());
    }
    
}
